class ArrayOps {
    private static int sum(String[] operands) {
        int result = 0;
        for(String operand : operands) 
            result += Integer.parseInt(operand);
        return result;
    }

    public static void main(String[] args) {
        for(String a : args) {
            System.out.println(a);
        }
        int sumresult = sum(args);
        System.out.println("sum: " + sumresult);
    }
}
